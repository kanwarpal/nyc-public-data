import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule } from '@angular/forms'; 
import {HttpModule} from '@angular/http';
import { AppComponent } from './app.component';
import {UsersComponent} from './users/users.component';
import { DataComponent } from './data/data.component';
import { SingleuserComponent } from './singleuser/singleuser.component';
import { LoginComponent } from './login/login.component';
import { RouterModule } from '@angular/router';
import { CitybookComponent } from './citybook/citybook.component'; // add import statement here
import {InputTextModule} from 'primeng/inputtext';
import {ButtonModule} from 'primeng/button';
import {InputSwitchModule} from 'primeng/inputswitch';
import {AutoCompleteModule} from 'primeng/autocomplete';

import { AgmCoreModule } from '@agm/core';
import { AgmSnazzyInfoWindowModule } from '@agm/snazzy-info-window';
import { ChartsComponent } from './charts/charts.component';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DataComponent,
    SingleuserComponent,
    LoginComponent,
    CitybookComponent,
    ChartsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InputTextModule,
    ButtonModule,
    InputSwitchModule,
    AutoCompleteModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyD2J1jXBnQmCe90NfKZ7rj4l-ybMR2h0Wo'
      }),

      AgmSnazzyInfoWindowModule,
      ChartsModule,
    
    RouterModule.forRoot([
      {
          path: 'login',
          component: LoginComponent
      },
      {
          path: 'admin',
          component: UsersComponent
      },
      {
          path: '',
          component: LoginComponent
      },
      {
          path: 'citybook',
          component: CitybookComponent
      },
      {
          path: 'data',
          component: DataComponent
      }

  ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
