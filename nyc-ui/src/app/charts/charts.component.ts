import { Component, OnInit, Input } from '@angular/core';
import { ChartsModule} from 'ng2-charts';
import {Http} from '@angular/http'
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

@Input('datum')
datum: any;
dataInit: any;
public doughnutChartLabels:string[];
public doughnutChartData:number[] = [350, 450, 100];
public doughnutChartType:string = 'doughnut';

public polarAreaChartLabels:string[]=["PROMOTED", "BATHROOM AND PLUMBING SUPPLIES: MISC.", "APPOINTED", "RETIRED", "DECREASE", "SMD BIRCH DOORS  FLUSH PARTICLE BOARD", "RESIGNED", "INCREASE"];
public polarAreaChartData:number[] = [300, 500, 100, 40, 120];
public polarAreaLegend:boolean = true;
public polarAreaChartType:string = 'polarArea';
  constructor(
    public http: Http
  ) { }

  ngOnInit() {this.http.get('https://data.cityofnewyork.us/resource/buex-bi6w.json')
 .subscribe(response => {
        this.dataInit = response.json();
        var section_data=this.countMe("section_name")
        var agency_data=this.countMe("agency_name");
        var short_title_data=this.countMe("short_title");
        for(var k=0;k<agency_data.labels.length;k++){
            if(agency_data.data[k]<100){
              agency_data.data.splice(k,1);
              agency_data.labels.splice(k,1);
            }
        }
        console.log(agency_data)
        console.log(section_data)

        console.log(short_title_data)
        this.doughnut(section_data);
        this.barChart(agency_data)
        this.polarchart(short_title_data)
      });
    }

  countMe(key){   
    var labels=[];
    var data=[];
    var counts={};
    this.dataInit.forEach(element => {
        var value=element[key];
        if(value in counts){
            counts[value]+=1;
        }else{
          counts[value]=0; //key generate
          labels.push(value);
        }
    });
    for(var k in counts){
          data.push(counts[k])
    }
      return {"labels":labels,"data":data};

  }

  doughnut(data){
    console.log(data.labels)
   this.doughnutChartLabels=data.labels;
   this.doughnutChartData=data.data;

  }




 // PolarAreapola
 polarchart(data){
    

    this.polarAreaLegend = true;
    var chartdata=[]
    for(var x=0;x<this.polarAreaChartLabels.length;x++){
      var index = data.labels.indexOf(this.polarAreaChartLabels[x])
      chartdata.push(data.data[index])
    }
    console.log(chartdata)
    this.polarAreaChartData=chartdata;



 }


  // bar
  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;
 
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'}
   
  ];
  barChart(data){ 
      this.barChartData = [{ data: data.data }];
    
     
  }
// 

  
}
