import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitybookComponent } from './citybook.component';

describe('CitybookComponent', () => {
  let component: CitybookComponent;
  let fixture: ComponentFixture<CitybookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitybookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitybookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
