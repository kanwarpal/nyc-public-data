import { Component, OnInit, Input } from '@angular/core';
import {Router} from '@angular/router';
@Component({
  selector: 'app-citybook',
  templateUrl: './citybook.component.html',
  styleUrls: ['./citybook.component.css']
})
export class CitybookComponent implements OnInit {
 @Input('whoIsuser')
 whoIsuser: any;

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
    if(this.whoIsuser === 'admin'){
      this.router.navigateByUrl('/admin');
    }
    else
    this.router.navigateByUrl('/data');
   
  }

}
