import { Component, OnInit  } from '@angular/core';
import {Http} from '@angular/http';

import {Router} from '@angular/router';


@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css','../../../node_modules/primeng/resources/themes/omega/theme.css','../../../node_modules/primeng/resources/primeng.min.css']
})
export class DataComponent implements OnInit  {

data: any;
public dataInit: any;
property: any;
checked: any;
texts: string[];
word: any;
edited: any;
employee:any;
results: string[];
description:any;
latlongdata:any;
address:any;
lat: number = 51.678418;
  lng: number = 7.809007;
  constructor(
    private http: Http,
    public router: Router,
    
    
  ) { }

  ngOnInit() {this.http.get('https://data.cityofnewyork.us/resource/buex-bi6w.json')
  .toPromise()
  .then(response => this.dataInit = response.json());

    this.edited = false
  }
  
openMe(request_id){
 
  
  
  this.dataInit.forEach(element => {
    if(element['request_id'] == request_id ){
      this.description = element;
      
    }
    
  });
  this.edited=true
  this.addDescription(this.description);
  this.getLatLong(this.description.vendor_address)
  

}
addDescription(description){
  var desc={}
  if(description['additional_description_1']){
    var  x = description['additional_description_1'];
    x=x.split(";")
    for(var k in x){
      var val=x[k].split(":");
      val[0]=val[0].replace(/ /g,"_").toLowerCase();
      desc[val[0]]=val[1]
    }
    console.log(desc)
  }
  this.employee=[desc];
}

 filter_search(word){

        var view_box=[];
   
        if(this.dataInit!=undefined){
          this.dataInit.forEach(element => {
            // for(var key in element){
            
            // }
            if(word !=undefined){
              if(element['agency_name'].toLowerCase().search(word.toLowerCase())>=0){
                view_box.push(element);
              }
            }
          });
         }
        
        return view_box;

  }
      
      getData(){
    this.http.get('https://data.cityofnewyork.us/resource/buex-bi6w.json')
    .toPromise()
    .then(response => this.data = response.json());
      var keys = [];
      var view_box=[];
     
      
      this.data.forEach(element => {
        // for(var key in element){
        
        // }
        for (var key in element) {
          if (element.hasOwnProperty(key)) {
             
          }
      }
       
      });
}
funX(){
  this.router.navigateByUrl('/citybook');

  console.log('map is pressed! ')
}
getLatLong(term){
  
  this.http.get('http://maps.google.com/maps/api/geocode/json?address=' + term + 'CA&sensor=false')
  .subscribe(data => {
    var json_data=data.json()
    var address=json_data.results[0].geometry;  
    this.lat=address.location.lat;
    this.lng=address.location.lng;
    this.address=term;
  });
}

  
}
// search(event) {
//   this.mylookupservice.getResults(event.query).then(data => {
//       this.results = data;
//   });
// }

