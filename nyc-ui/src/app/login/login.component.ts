import { Component, OnInit } from '@angular/core';
import { Http} from '@angular/http';
import {Router} from '@angular/router';
import {environment} from '..//../environments/environment'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public users: any;
  public user_new: any;
  public whoIsuser: any;
  public isRegisteredUser: boolean;
  public object_user = {
  "userName": '',
  "firstName": '',
  "lastName": ''
  };
    constructor(
      public http: Http, public router: Router
    ) { }

    ngOnInit() {
      this.isRegisteredUser=false;
      this.users=[];
      this.http.get(`${environment.apihost}/users/users`).toPromise()
  .then(response => this.user_new = response.json());
    }
  
  cambiar_sign_up() {
    document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
    let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
    element_WITH_cont_form_sign_up[0].style.display = "block";
    let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
    element_WITH_cont_form_login[0].style.opacity = "0";
     setTimeout(function(){  
        let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_sign_up[0].style.opacity = "1";},100);  
      setTimeout(function(){   
        let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_login[0].style.display = "none";},400); 
  }

  
  ocultar_login_sign_up() {
    document.querySelector('.cont_forms').className = "cont_forms";  
    let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
    element_WITH_cont_form_sign_up[0].style.opacity = "0";               
    let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
    element_WITH_cont_form_login[0].style.opacity = "0"; 
      setTimeout(function(){
        let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_sign_up[0].style.display = "none";
        let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_login[0].style.display = "none";
      },500);             
  }


  cambiar_login() {
      document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";  
      let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
      element_WITH_cont_form_login[0].style.display = "block";
      let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
      element_WITH_cont_form_sign_up[0].style.opacity = "0";               
      setTimeout(function(){ 
        let element_WITH_cont_form_login = document.getElementsByClassName("cont_form_login") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_login[0].style.opacity = "1"; },400);  
      setTimeout(function(){    
        let element_WITH_cont_form_sign_up = document.getElementsByClassName("cont_form_sign_up") as HTMLCollectionOf<HTMLElement>;
        element_WITH_cont_form_sign_up[0].style.display = "none";
      },200);  
  }

  addUser() {
    console.log('Adding:'+this.object_user.firstName);
    return this.http.post(`${environment.apihost}/users/users`, this.object_user).subscribe(response=>{
     
      
      this.users.push(response.json());
      this.object_user.userName='';
      this.object_user.firstName='';
      this.object_user.lastName='';})
      
     

  }
  funX(){
    this.router.navigateByUrl('/admin');
  }
is_Registed_user(){
  var GreetingName;
  
    console.log(this.user_new)
    this.user_new.forEach(element => {
    if(element.userName === this.object_user.userName){
        // this.router.navigateByUrl('/admin');
     GreetingName = element.firstName
        this.isRegisteredUser = true
      }
    } );
    if(this.isRegisteredUser){
        window.alert('Hi dear '+ GreetingName)
        this.whoIsuser = this.object_user.userName
      }
       else{
        window.alert('you are nota registerd user :'+ this.object_user.userName)
      }
      this.object_user.userName=""
   

}

}
