import { Component, OnInit, Input } from '@angular/core';
import { Http } from '@angular/http';
import {environment} from '..//../environments/environment'
@Component({
  selector: 'app-singleuser',
  templateUrl: './singleuser.component.html',
  styleUrls: ['./singleuser.component.css']
})
export class SingleuserComponent implements OnInit {
  @Input('user')
  user: any;
  private modal;
  private userName;
  private firstName;
  private lastName;
  editUserFlag: boolean;

  constructor(
    private http: Http
  ) { 
    this.editUserFlag=false;
  }

  ngOnInit() {
  }

  deleteUser() {
    return this.http.delete(`${environment.apihost}/users/users/${this.user.id}`).subscribe(response=>this.user=null);
    
  }

  UpdateUser(){    
    console.log(this.user.id, this.user.userName, this.user.firstName,this.user.lastName);
    this.http.patch(`${environment.apihost}/users/users/${this.user.id}`,{
      "userName": this.user.userName,
      "firstName":this.user.firstName,
      "lastName":this.user.lastName
    })
  .subscribe(res=>{      
      this.user.userName=res.json().userName;
      this.user.firstName=res.json().firstName;
      this.user.lastName=res.json().lastName;
      this.editUserFlag=false;
    })   
  }

  editUser(){
    this.editUserFlag=true;
  }
}