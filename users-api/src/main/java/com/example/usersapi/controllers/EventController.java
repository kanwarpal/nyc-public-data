package com.example.usersapi.controllers;


import com.example.usersapi.models.Event;
import com.example.usersapi.repositories.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EventController {

    @Autowired
    private EventRepository eventRepository;

    @GetMapping("/events")
    public Iterable<Event> findAllEvents(){
        return eventRepository.findAll();
    }

    @GetMapping("/events/string")
    public String findString(){
        return "test string" ;
    }

}
